GSHHG for Debian
----------------

The GSHHG data set is composed of three different datasets, and it's
distributed in NetCDF 4 format, ESRI shapefile and native binary files.

The gmt-gshhg Debian package includes the GSHHG data set in NetCDF 4 format
for use with GMT 5. It succeeds the gmt-gshhs package, the upstream project
was renamed starting with version 2.2.1.

Regarding the composition of the GSHHG data set, the projects homepage [1]
documents the following:

"
 We present a high-resolution geography data set amalgamated from three
 data bases in the public domain:

  1.  World Vector Shorelines (WVS).
      http://shoreline.noaa.gov/data/datasheets/wvs.html
  2.  CIA World Data Bank II (WDBII).
      http://www.evl.uic.edu/pape/data/WDB
  3.  Atlas of the Cryosphere (AC).
      http://nsidc.org/data/atlas/

 The WVS is our basis for shorelines except for Antarctica while the
 WDBII is the basis for lakes, although there are instances where
 differences in coastline representations necessitated adding WDBII
 islands to GSHHG. The WDBII source also provides all political borders
 and rivers. The addition of AC since 2.3.0 allows us to offer two
 choices for Antarctica coastlines: Ice-front or Grounding line. These
 are encoded as levels 5 and 6, respectively and users of GSHHG can
 choose which set to use. GSHHG data have undergone extensive
 processing and should be free of internal inconsistencies such as
 erratic points and crossing segments. The shorelines are constructed
 entirely from hierarchically arranged closed polygons.
"

[1] http://www.soest.hawaii.edu/pwessel/gshhg/index.html

 -- Bas Couwenberg <sebastic@debian.org>  Thu, 18 Jun 2015 23:14:11 +0200
